This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## CRA Template for Class Components and Hooks Including Custom Hooks

### Create React App with this template

`npx create-react-app <your-project-name> --template base-hooks-hocs`
